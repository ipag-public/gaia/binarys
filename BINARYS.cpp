#include "binarys.hpp"
using namespace density;

template<class Type>
Type objective_function<Type>::operator() ()
{
  // Radial Velocity data of the primary
  DATA_INTEGER(doVr);
  DATA_VECTOR(Vr);
  DATA_VECTOR(sigVr);
  DATA_VECTOR(timeVr);
  DATA_VECTOR(VrEpoch); // in case different shifts in RV for different epochs.
  // Radial velocity data of the secondary
  DATA_INTEGER(doVr2);
  DATA_VECTOR(Vr2);
  DATA_VECTOR(sigVr2);
  DATA_VECTOR(timeVr2);
  DATA_VECTOR(VrEpoch2); // in case different shifts in RV for different epochs.
  // Direct Imaging data
  DATA_INTEGER(doDI);
  DATA_VECTOR(ksi21);
  DATA_VECTOR(eta21);
  DATA_VECTOR(sigksi21);
  DATA_VECTOR(sigeta21);
  DATA_VECTOR(corksieta21);
  DATA_VECTOR(timeDi);
  //  reference astrometric data
  DATA_SCALAR(Sra);
  DATA_SCALAR(Sdec);
  DATA_SCALAR(Splx);
  DATA_SCALAR(Smualpha);
  DATA_SCALAR(Smudelta);
  // Hipparcos astrometric data
  DATA_INTEGER(doIAD);
  DATA_VECTOR(Hip_Ares);
  DATA_VECTOR(Hip_Asig);
  DATA_VECTOR(Hip_dpra_nu);
  DATA_VECTOR(Hip_dpdec_nu);
  DATA_VECTOR(Hip_dppi_nu);
  DATA_VECTOR(Hip_dpmura_nu);
  DATA_VECTOR(Hip_dpmudec_nu);
  DATA_VECTOR(Hip_epoch);  
  // Transit Data HIP1
  DATA_INTEGER(doTD1);
  DATA_VECTOR(Hip_b1);
  DATA_VECTOR(Hip_sb1);
  DATA_VECTOR(Hip_b2);
  DATA_VECTOR(Hip_sb2);
  DATA_VECTOR(Hip_b3);
  DATA_VECTOR(Hip_sb3);
  DATA_VECTOR(Hip_b4);
  DATA_VECTOR(Hip_sb4);
  DATA_VECTOR(Hip_b5);
  DATA_VECTOR(Hip_sb5);
  DATA_VECTOR(Hip_fx);
  DATA_VECTOR(Hip_fy);
  DATA_VECTOR(Hip_fp);
  // Transit Data HIP2
  DATA_INTEGER(doTD2);
  DATA_INTEGER(doSigFactor);
  DATA_VECTOR(Hip_HacHdc);
  DATA_VECTOR(Hip_sHacHdc);
  DATA_VECTOR(Hip_Beta4);
  DATA_VECTOR(Hip_sBeta4);
  DATA_VECTOR(Hip_Beta5);
  DATA_VECTOR(Hip_sBeta5);
  // Adjust Variability
  DATA_INTEGER(doVAR);
  DATA_VECTOR(Hp);
  DATA_INTEGER(meanHp);
  // Gaia 
  DATA_SCALAR(GaiaEpoch);
  // Gaia astrometric data DR2 or EDR3
  DATA_INTEGER(doGDR);
  DATA_INTEGER(solGAP);
  DATA_VECTOR(GDRAP);
  DATA_VECTOR(GDRAPsig);
  DATA_MATRIX(GDRcormat);
  DATA_VECTOR(Gfake_time);
  DATA_VECTOR(Gfake_dpra_nu);
  DATA_VECTOR(Gfake_dpdec_nu);
  DATA_VECTOR(Gfake_dppi_nu);
  DATA_VECTOR(Gfake_dpmura_nu);
  DATA_VECTOR(Gfake_dpmudec_nu);
  DATA_SCALAR(GDRAVrUsed); // Vr used by the Gaia astrometric solution for the perspective acceleration, if any.
  // Gaia B: resolved star
  DATA_INTEGER(solGAPB);
  DATA_VECTOR(GDRAPB);
  DATA_MATRIX(GDRcormatB);
  DATA_VECTOR(GDRAPsigB);
  DATA_SCALAR(GDRBVrUsed); // Vr used by the Gaia astrometric solution for the perspective acceleration, if any.
  // Gaia DR3 NSS astrometric solution
  DATA_SCALAR(bitindexA);
  DATA_VECTOR(nssAparams); // ra, dec, parallax, pmra, pmdec, a_thiele_innes, b_thiele_innes, f_thiele_innes, g_thiele_innes, c_thiele_innes, h_thiele_innes, center_of_mass_velocity, eccentricity, period, t_periastron (following bit_index order)
  DATA_MATRIX(nssAcovmat); // same order
  // Gaia DR3 NSS spectroscopic solution
  DATA_SCALAR(bitindexS);
  DATA_VECTOR(nssSparams); // period, center_of_mass_velocity, semi_amplitude_primary, semi_amplitude_secondary, eccentricity, arg_periastron, t_periastron (following bit_index order)
  DATA_MATRIX(nssScovmat); // same order
  // Hipparcos parallax zero point
  DATA_SCALAR(HipPlxZeroPt);
  // Gaia DR4 epoch astrometry
  DATA_INTEGER(doGaiaEpoch);
  DATA_VECTOR(Gaia_time);
  DATA_VECTOR(Gaia_dpra_nu);
  DATA_VECTOR(Gaia_dpdec_nu);
  DATA_VECTOR(Gaia_dppi_nu);
  DATA_VECTOR(Gaia_dpmura_nu);
  DATA_VECTOR(Gaia_dpmudec_nu);
  DATA_VECTOR(Gaia_centroid_pos_al);
  DATA_VECTOR(Gaia_centroid_pos_error_al);
  DATA_SCALAR(Gaia_ra0);
  DATA_SCALAR(Gaia_dec0);
  // Chose set of data to adjust
  DATA_INTEGER(parset);
  
  // PARAMETERS that can be fitted by BINARYS 
  PARAMETER(a1);
  PARAMETER(periodYrs);
  PARAMETER(relTP);
  PARAMETER(ecc);
  PARAMETER(omega2);
  PARAMETER(inclination);
  PARAMETER(nodeangle);
  PARAMETER(par1); // parset=1: M1 - parset=2: a21
  PARAMETER(Vr0);
  PARAMETER(VrEpochShift); // 2/1
  PARAMETER(VrEpochShift2); // 3/1
  PARAMETER(VrEpochShift3); // 4/1
  PARAMETER(VrVarJitter);
  // Astrometry 5P versus reference solution
  PARAMETER(dra);
  PARAMETER(ddec);
  PARAMETER(dplx);
  PARAMETER(dpmra);
  PARAMETER(dpmdec);
  // Flux fraction
  PARAMETER(beta);
  // Hipparcos A1 flux
  PARAMETER(Hip_A1);

  Type massRatio;
  if (parset==1) {
    Type M1 = par1;
    massRatio = getqfroma1PM1(a1,periodYrs,M1);
  } else {
    Type a21 = par1;
    massRatio = 1/(a21/a1-1);
  }
  Type period = periodYrs*JY2D;
  Type a2 = a1/massRatio;
  Type timeperiastron = relTP * period;
  Type B = massRatio / (1+massRatio); // M2/(M1+M2)
  Type APra = (dra*MAS2DEG)/cos(Sdec*DEG2RAD)+Sra;
  Type APdec = ddec*MAS2DEG+Sdec;
  Type APpmra = dpmra+Smualpha;
  Type APpmdec = dpmdec+Smudelta;
  Type APplx = Splx + dplx;
  Type APRefEpoch;
  if (doIAD>0 | doTD1>0 | doTD2>0) { // reference epoch is Hipparcos
    APRefEpoch = HIP_EPOCH;
  } else { // reference epoch is Gaia
    APRefEpoch = GaiaEpoch;
  }

  Type nll = 0;

  // Direct Imaging
  if (doDI==1) {
    for(int i=0; i<timeDi.size();i++) {
      vector<Type> relposDI = relpos(period,timeperiastron,ecc,omega2,inclination,nodeangle,a2,massRatio,timeDi[i]);
      matrix<Type> DIcov(2,2);
      DIcov(0,0) = 1;
      DIcov(0,1) = corksieta21[i];
      DIcov(1,0) = corksieta21[i];
      DIcov(1,1) = 1;
      MVNORM_t<Type> neglogdmvnorm(DIcov);
      vector<Type> DIresiduals(2);
      DIresiduals[0] = (ksi21[i]-(relposDI[4]-relposDI[0])*APplx)/sigksi21[i];
      DIresiduals[1] = (eta21[i]-(relposDI[5]-relposDI[1])*APplx)/sigeta21[i];
      nll += neglogdmvnorm(DIresiduals);
    }
  }
  // Radial Velocity
  if (doVr==1) {
    Type barycenterVr = Vr0;
    for(int i=0; i<timeVr.size();i++) {
      // Take into account barycentre radial velocity change for very nearby stars.
      if (APRefEpoch>0) {
        vector<Type> epochAP = epochpropaFull(APra,APdec,APplx,APpmra,APpmdec,Vr0,APRefEpoch,timeVr[i]/JY2D+2000);
        barycenterVr = epochAP[5];
      }
      vector<Type> relposVR = relpos(period,timeperiastron,ecc,omega2,inclination,nodeangle,a2,massRatio,timeVr[i]);
      Type Vrref = barycenterVr;
      if (VrEpoch[i]==2) {
        Vrref += VrEpochShift;
      } else if (VrEpoch[i]==3) {
        Vrref += VrEpochShift2;
      } else if (VrEpoch[i]==4) {
        Vrref += VrEpochShift3;
      }
      nll -= dnorm(Vr[i],relposVR[3]+Vrref,sqrt(sigVr[i]*sigVr[i]+VrVarJitter),true);
    }
  }
  if (doVr2==1) {
    Type barycenterVr = Vr0;
    for(int i=0; i<timeVr2.size();i++) {
      // Take into account barycentre radial velocity change for very nearby stars.
      if (APRefEpoch>0) {
        vector<Type> epochAP = epochpropaFull(APra,APdec,APplx,APpmra,APpmdec,Vr0,APRefEpoch,timeVr2[i]/JY2D+2000);
        barycenterVr = epochAP[5];
      }
      vector<Type> relposVR2 = relpos(period,timeperiastron,ecc,omega2,inclination,nodeangle,a2,massRatio,timeVr2[i]);
      Type Vrref = barycenterVr;
      if (VrEpoch2[i]==2) {
        Vrref += VrEpochShift;
      } else if (VrEpoch2[i]==3) {
        Vrref += VrEpochShift2;
      } else if (VrEpoch2[i]==4) {
        Vrref += VrEpochShift3;
      }
      nll -= dnorm(Vr2[i],relposVR2[7]+Vrref,sigVr2[i],true);
    }
  }
  // Hipparcos
  if (doIAD==1 | doTD2==1) {
    Type r0 = beta/(1-beta); 
    Type r;
    for(int i=0; i<Hip_Ares.size();i++) {
      if (doVAR==0) {
        r = r0;
      }
      if (doVAR==1) {
        r = pow( ((1+r0)/r0)*pow(10,-Type(0.4)*(Hp[i]-meanHp))-1 , -1.);
      }
      if (doVAR==2) {
        r = (1+r0)*pow(10,-Type(0.4)*(Hp[i]-meanHp)) - 1.;
      }
      Type betat = r/(1+r);
      vector<Type> relposIAD = relpos(period,timeperiastron,ecc,omega2,inclination,nodeangle,a2,massRatio,(Hip_epoch[i]+HIP_EPOCH-APRefEpoch)*JY2D);
      Type deltaKsi = (relposIAD[4]-relposIAD[0])*APplx;
      Type deltaEta = (relposIAD[5]-relposIAD[1])*APplx;
      Type rhoP = Hip_dpra_nu[i]*deltaKsi+Hip_dpdec_nu[i]*deltaEta;
      Type ksi = 2*M_PI*modulo1(rhoP/HIP_MODGRIDPERIOD);
      Type u = betat*sin(ksi)/(1 - betat + betat*cos(ksi));
      Type phi = 2.*atan(u/(1 + sqrt(1+u*u))); // = atan2...
      Type Ashift = phi*HIP_MODGRIDPERIOD/(2*M_PI) - B*rhoP;
      Type newAres = Ashift+Hip_dpra_nu[i]*dra+Hip_dpdec_nu[i]*ddec+Hip_dppi_nu[i]*(dplx+HipPlxZeroPt)+Hip_dpmura_nu[i]*dpmra+Hip_dpmudec_nu[i]*dpmdec;
      // perspective acceleration
      Type mur = Vr0*APplx/AZ;
      newAres += mur*Hip_epoch[i]*(Hip_dppi_nu[i]*(APplx+HipPlxZeroPt)+Hip_dpmura_nu[i]*APpmra+Hip_dpmudec_nu[i]*APpmdec);
      if (doSigFactor==1) {
        Type sigFactor = (1+r)/sqrt(1+2*r*cos(ksi)+r*r); // Eq. 4.29 and 2.47
        nll -= dnorm(Hip_Ares[i], newAres, Hip_Asig[i]*sigFactor, true);
      }
      if (doSigFactor==0){
        nll -= dnorm(Hip_Ares[i], newAres, Hip_Asig[i], true);
      }
      if (doTD2==1) {
        Type nf = pow(1+2*r*cos(ksi)+r*r,3./2.);
        Type HacHdcSim = -2.5*log10(sqrt(1+2*r*cos(ksi)+r*r)/(1+r));
        Type beta4Sim = (1+(r+r*r)*(2*cos(ksi)+cos(2*ksi))+r*r*r)/nf;
        Type beta5Sim = (r-r*r)*(2*sin(ksi)-sin(2*ksi))/nf;
        nll -= dnorm(Hip_HacHdc[i],HacHdcSim,Hip_sHacHdc[i],true);
        nll -= dnorm(Hip_Beta4[i],beta4Sim,Hip_sBeta4[i],true);
        nll -= dnorm(Hip_Beta5[i],beta5Sim,Hip_sBeta5[i],true);
      }
    }
  }
  // TD1
  if (doTD1==1){
    Type Hip_A2 = Hip_A1*beta/(1-beta);
    for(int i=0; i<Hip_fx.size();i++) {
      vector<Type> relposTD = relpos(period,timeperiastron,ecc,omega2,inclination,nodeangle,a2,massRatio,(Hip_epoch[i]+HIP_EPOCH-APRefEpoch)*JY2D);
      Type p1 = (Hip_fx[i]*(dra + relposTD[0]*APplx + Hip_epoch[i]*dpmra) + Hip_fy[i]*(ddec + relposTD[1]*APplx + Hip_epoch[i]*dpmdec) + Hip_fp[i]*(dplx+HipPlxZeroPt))*MAS2RAD;
      Type p2 = (Hip_fx[i]*(dra + relposTD[4]*APplx + Hip_epoch[i]*dpmra) + Hip_fy[i]*(ddec + relposTD[5]*APplx + Hip_epoch[i]*dpmdec) + Hip_fp[i]*(dplx+HipPlxZeroPt))*MAS2RAD;
      Type b1Sim = Hip_A1 + Hip_A2;
      Type b2Sim = TD_M1 * (Hip_A1*cos(p1) + Hip_A2*cos(p2));
      Type b3Sim = -TD_M1 * (Hip_A1*sin(p1)+ Hip_A2*sin(p2));
      Type b4Sim = TD_M2 * (Hip_A1*cos(2.*p1) + Hip_A2*cos(2.*p2));
      Type b5Sim = -TD_M2 * (Hip_A1*sin(2.*p1) + Hip_A2*sin(2.*p2));
      nll -= dnorm(Hip_b1[i],b1Sim,Hip_sb1[i],true);
      nll -= dnorm(Hip_b2[i],b2Sim,Hip_sb2[i],true);
      nll -= dnorm(Hip_b3[i],b3Sim,Hip_sb3[i],true);
      nll -= dnorm(Hip_b4[i],b4Sim,Hip_sb4[i],true);
      nll -= dnorm(Hip_b5[i],b5Sim,Hip_sb5[i],true);
    }
  }
  // Gaia
  if (doGDR==1) {
    vector<Type> Gfake_Ares(Gfake_time.size());
    vector<Type> gaiatrueAP = epochpropaFull(APra,APdec,APplx,APpmra,APpmdec,Vr0,APRefEpoch,GaiaEpoch);
    for(int i=0; i<Gfake_time.size(); i++) {
      vector<Type> relposGaia = relpos(period,timeperiastron,ecc,omega2,inclination,nodeangle,a2,massRatio,Gfake_time[i]);
      Gfake_Ares[i] = Gfake_dpra_nu[i]*(beta-B)*(relposGaia[4]-relposGaia[0])*APplx+Gfake_dpdec_nu[i]*(beta-B)*(relposGaia[5]-relposGaia[1])*APplx;
      // Add perspective acceleration
      Type mur = (gaiatrueAP[5]-GDRAVrUsed)*gaiatrueAP[2]/AZ;
      Gfake_Ares[i] += - mur*(Gfake_time[i]/JY2D+2000-GaiaEpoch)*(Gfake_dppi_nu[i]*gaiatrueAP[2]+Gfake_dpmura_nu[i]*gaiatrueAP[3]+Gfake_dpmudec_nu[i]*gaiatrueAP[4]);
    }
    vector<Type> gaiares = getIADSolution(Gfake_Ares, Gfake_dpra_nu, Gfake_dpdec_nu, Gfake_dppi_nu, Gfake_dpmura_nu, Gfake_dpmudec_nu);
    vector<Type> gaiaresidualAP(5);
    gaiaresidualAP[0] = ((gaiatrueAP[0]-GDRAP[0])*cos(gaiatrueAP[1]*DEG2RAD)/MAS2DEG + gaiares[0])/GDRAPsig[0];
    gaiaresidualAP[1] = ((gaiatrueAP[1]-GDRAP[1])/MAS2DEG + gaiares[1])/GDRAPsig[1];
    gaiaresidualAP[2] = (gaiatrueAP[2]-GDRAP[2] + gaiares[2])/GDRAPsig[2];
    gaiaresidualAP[3] = (gaiatrueAP[3]-GDRAP[3] + gaiares[3])/GDRAPsig[3];
    gaiaresidualAP[4] = (gaiatrueAP[4]-GDRAP[4] + gaiares[4])/GDRAPsig[4];
    if (solGAP==2) {
      vector<Type> gaiaresidualRADEC(2);
      gaiaresidualRADEC[0] = gaiaresidualAP[0];
      gaiaresidualRADEC[1] = gaiaresidualAP[1];
      matrix<Type> RADECcormat(2,2);
      RADECcormat(0,0) = 1;
      RADECcormat(0,1) = GDRcormat(0,1);
      RADECcormat(1,0) = GDRcormat(1,0);
      RADECcormat(1,1) = 1;
      density::MVNORM_t<Type> GDR2neglogdmvnormRADEC(RADECcormat);
      nll += GDR2neglogdmvnormRADEC(gaiaresidualRADEC);
    } else {
      density::MVNORM_t<Type> GDR2neglogdmvnorm(GDRcormat);
      nll += GDR2neglogdmvnorm(gaiaresidualAP);
    }
  }

  if (doGDR==2) {
    vector<Type> Gfake_AresA(Gfake_time.size());
    vector<Type> Gfake_AresB(Gfake_time.size());
    vector<Type> gaiatrueAP = epochpropaFull(APra,APdec,APplx,APpmra,APpmdec,Vr0,APRefEpoch,GaiaEpoch);
    for(int i=0; i<Gfake_time.size(); i++) {
      vector<Type> relposGaia = relpos(period,timeperiastron,ecc,omega2,inclination,nodeangle,a2,massRatio,Gfake_time[i]);
      Gfake_AresA[i] = Gfake_dpra_nu[i]*relposGaia[0]*APplx+Gfake_dpdec_nu[i]*relposGaia[1]*APplx; //a1
      Gfake_AresB[i] = Gfake_dpra_nu[i]*relposGaia[4]*APplx+Gfake_dpdec_nu[i]*relposGaia[5]*APplx; //a2
      // Add perspective acceleration
      Type murA = (gaiatrueAP[5]-GDRAVrUsed)*gaiatrueAP[2]/AZ;
      Type murB = (gaiatrueAP[5]-GDRBVrUsed)*gaiatrueAP[2]/AZ;
      Gfake_AresA[i] += -murA*(Gfake_time[i]/JY2D+2000-GaiaEpoch)*(Gfake_dppi_nu[i]*gaiatrueAP[2]+Gfake_dpmura_nu[i]*gaiatrueAP[3]+Gfake_dpmudec_nu[i]*gaiatrueAP[4]);
      Gfake_AresB[i] += -murB*(Gfake_time[i]/JY2D+2000-GaiaEpoch)*(Gfake_dppi_nu[i]*gaiatrueAP[2]+Gfake_dpmura_nu[i]*gaiatrueAP[3]+Gfake_dpmudec_nu[i]*gaiatrueAP[4]);
    }
    vector<Type> gaiaresA = getIADSolution(Gfake_AresA, Gfake_dpra_nu, Gfake_dpdec_nu, Gfake_dppi_nu, Gfake_dpmura_nu, Gfake_dpmudec_nu);
    vector<Type> gaiaresB = getIADSolution(Gfake_AresB, Gfake_dpra_nu, Gfake_dpdec_nu, Gfake_dppi_nu, Gfake_dpmura_nu, Gfake_dpmudec_nu);
    vector<Type> gaiaresidualAPA(5);
    vector<Type> gaiaresidualAPB(5);

    gaiaresidualAPA[0] = ((gaiatrueAP[0]-GDRAP[0])*cos(gaiatrueAP[1]*DEG2RAD)/MAS2DEG + gaiaresA[0])/GDRAPsig[0];
    gaiaresidualAPA[1] = ((gaiatrueAP[1]-GDRAP[1])/MAS2DEG + gaiaresA[1])/GDRAPsig[1];
    gaiaresidualAPA[2] = (gaiatrueAP[2]-GDRAP[2] + gaiaresA[2])/GDRAPsig[2];
    gaiaresidualAPA[3] = (gaiatrueAP[3]-GDRAP[3] + gaiaresA[3])/GDRAPsig[3];
    gaiaresidualAPA[4] = (gaiatrueAP[4]-GDRAP[4] + gaiaresA[4])/GDRAPsig[4];

    gaiaresidualAPB[0] = ((gaiatrueAP[0]-GDRAPB[0])*cos(gaiatrueAP[1]*DEG2RAD)/MAS2DEG + gaiaresB[0])/GDRAPsigB[0];
    gaiaresidualAPB[1] = ((gaiatrueAP[1]-GDRAPB[1])/MAS2DEG + gaiaresB[1])/GDRAPsigB[1];
    gaiaresidualAPB[2] = (gaiatrueAP[2]-GDRAPB[2] + gaiaresB[2])/GDRAPsigB[2];
    gaiaresidualAPB[3] = (gaiatrueAP[3]-GDRAPB[3] + gaiaresB[3])/GDRAPsigB[3];
    gaiaresidualAPB[4] = (gaiatrueAP[4]-GDRAPB[4] + gaiaresB[4])/GDRAPsigB[4];

    if (solGAP==2) {
      vector<Type> gaiaresidualRADEC(2);
      gaiaresidualRADEC[0] = gaiaresidualAPA[0];
      gaiaresidualRADEC[1] = gaiaresidualAPA[1];
      matrix<Type> RADECcormat(2,2);
      RADECcormat(0,0) = 1;
      RADECcormat(0,1) = GDRcormat(0,1);
      RADECcormat(1,0) = GDRcormat(1,0);
      RADECcormat(1,1) = 1;
      density::MVNORM_t<Type> GDR2neglogdmvnormRADEC(RADECcormat);
      nll += GDR2neglogdmvnormRADEC(gaiaresidualRADEC);
    } else {
      density::MVNORM_t<Type> GDR2neglogdmvnorm(GDRcormat);
      nll += GDR2neglogdmvnorm(gaiaresidualAPA);
    }
    if (solGAPB==2) {
      vector<Type> gaiaresidualRADECB(2);
      gaiaresidualRADECB[0] = gaiaresidualAPB[0];
      gaiaresidualRADECB[1] = gaiaresidualAPB[1];
      matrix<Type> RADECcormatB(2,2);
      RADECcormatB(0,0) = 1;
      RADECcormatB(0,1) = GDRcormatB(0,1);
      RADECcormatB(1,0) = GDRcormatB(1,0);
      RADECcormatB(1,1) = 1;
      density::MVNORM_t<Type> GDR2neglogdmvnormRADEC(RADECcormatB);
      nll += GDR2neglogdmvnormRADEC(gaiaresidualRADECB);
    } else {
      density::MVNORM_t<Type> GDR2neglogdmvnormB(GDRcormatB);
      nll += GDR2neglogdmvnormB(gaiaresidualAPB);
    }
  }
  
  if (bitindexA!=0) { // Gaia NSS astrometric binary
    Type a0 = (massRatio/(1+massRatio)-beta)*(a1+a2)*APplx; // in mas
    Type cosi = cos(inclination);
    Type sini = sin(inclination);
    Type sinO = sin(omega2+M_PI);
    Type cosO = cos(omega2+M_PI);
    Type cosNA = cos(nodeangle);
    Type sinNA = sin(nodeangle);
    Type tiA = a0*(cosNA*cosO-sinNA*sinO*cosi);
    Type tiB = a0*(sinNA*cosO+cosNA*sinO*cosi);
    Type tiF = a0*(-cosNA*sinO-sinNA*cosO*cosi);
    Type tiG = a0*(-sinNA*sinO+cosNA*cosO*cosi);
    Type tiC = a1*sinO*sini; // a1 in AU
    Type tiH = a1*cosO*sini;
    // warning: for cases with fixed eccentricity (bitindex=65435), tiH should be adapted to omega=0
    vector<Type> TIresiduals(nssAparams.size());
    MVNORM_t<Type> neglogdmvnorm(nssAcovmat);
    vector<Type> gaiatrueAP = epochpropaFull(APra,APdec,APplx,APpmra,APpmdec,Vr0,APRefEpoch,GaiaEpoch);
    TIresiduals[0] =  (gaiatrueAP[0]-nssAparams[0])*cos(gaiatrueAP[1]*DEG2RAD)/MAS2DEG; // ra
    TIresiduals[1] = (gaiatrueAP[1]-nssAparams[1])/MAS2DEG; // dec
    TIresiduals[2] = (gaiatrueAP[2]-nssAparams[2]); // plx
    TIresiduals[3] = (gaiatrueAP[3]-nssAparams[3]); // pmra
    TIresiduals[4] = (gaiatrueAP[4]-nssAparams[4]); // pmdec
    int i=5;
    TIresiduals[i] = (tiA-nssAparams[i]); i++;
    TIresiduals[i] = (tiB-nssAparams[i]); i++;
    TIresiduals[i] = (tiF-nssAparams[i]); i++;
    if (CppAD::abs(bitindexA)==8191 || bitindexA==65535) {
      TIresiduals[i] = (tiG-nssAparams[i]); i++;
    }
    if (bitindexA==65535) {
      TIresiduals[i] = (tiC-nssAparams[i]); i++;
    }
    if (bitindexA==65535 || bitindexA==65435) {
      TIresiduals[i] = (tiH-nssAparams[i]); i++;
      TIresiduals[i] = (Vr0-nssAparams[i]); i++;
    }
    if (bitindexA==-8191) { // OrbitalAlternative or OrbitalTargeted
      TIresiduals[i] = (period-nssAparams[i]); i++;
      TIresiduals[i] = (ecc-nssAparams[i]); i++;
    } else {
      if (bitindexA==8191 || bitindexA==65535) { // Orbital and AstroSpectrosB1
        TIresiduals[i] = (ecc-nssAparams[i]); i++;
      }
      TIresiduals[i] = (period-nssAparams[i]); i++;
    }
    TIresiduals[i] = (timeperiastron+(2000-GaiaEpoch)*JY2D-nssAparams[i]); i++;
    nll += neglogdmvnorm(TIresiduals);
  }

  if (bitindexS!=0) { // Gaia NSS SB1 or SB2 binary
    vector<Type> gaiatrueAP = epochpropaFull(APra,APdec,APplx,APpmra,APpmdec,Vr0,APRefEpoch,GaiaEpoch);
    vector<Type> NSSresiduals(nssSparams.size());
    MVNORM_t<Type> neglogdmvnorm(nssScovmat);
    Type K1 = (AU_SI*1e-3*2*M_PI/DAYSEC * a1 * sin(inclination)) /(period * sqrt(1 - ecc*ecc));
    Type K2 = (AU_SI*1e-3*2*M_PI/DAYSEC * a2 * sin(inclination)) /(period * sqrt(1 - ecc*ecc));
    int i=0;
    NSSresiduals[i] = (period-nssSparams[i]); i++;
    NSSresiduals[i] = (gaiatrueAP[5]-nssSparams[i]); i++; // barycenter radial velocity
    NSSresiduals[i] = (K1-nssSparams[i]); i++;
    if (bitindexS==255 | bitindexS==63) { // SB2
      NSSresiduals[i] = (K2-nssSparams[i]); i++;
    }
    if (bitindexS!=31 & bitindexS!=63) { // not a C solution
      NSSresiduals[i] = (ecc-nssSparams[i]); i++;
      NSSresiduals[i] = omega2/DEG2RAD+180-nssSparams[i]; i++;
    }
    NSSresiduals[i] = (timeperiastron+(2000-GaiaEpoch)*JY2D-nssSparams[i]); i++;
    nll += neglogdmvnorm(NSSresiduals);
  }
  
  if (doGaiaEpoch==1) { // Gaia DR4 epoch astrometry
    vector<Type> gaiatrueAP = epochpropaFull(APra,APdec,APplx,APpmra,APpmdec,Vr0,APRefEpoch,GaiaEpoch);
    for(int i=0; i<Gaia_time.size(); i++) {
      vector<Type> relposGaia = relpos(period,timeperiastron,ecc,omega2,inclination,nodeangle,a2,massRatio,Gaia_time[i]);
      Type deltaKsi = (beta-B)*(relposGaia[4]-relposGaia[0])*APplx;
      Type deltaEta = (beta-B)*(relposGaia[5]-relposGaia[1])*APplx;
      Type deltaRa = (gaiatrueAP[0]-Gaia_ra0)*cos(gaiatrueAP[1]*DEG2RAD)/MAS2DEG;
      Type deltaDec = (gaiatrueAP[1]-Gaia_dec0)/MAS2DEG;
      Type gaiaAres = Gaia_dpra_nu[i]*(deltaRa+deltaKsi)+Gaia_dpdec_nu[i]*(deltaDec+deltaEta)+Gaia_dppi_nu[i]*gaiatrueAP[2]+Gaia_dpmura_nu[i]*gaiatrueAP[3]+Gaia_dpmudec_nu[i]*gaiatrueAP[4];
      // Add perspective acceleration
      Type mur = (gaiatrueAP[5]+relposGaia[3])*gaiatrueAP[2]/AZ;
      gaiaAres -= mur*(Gaia_time[i]/JY2D+2000-GaiaEpoch)*(Gaia_dppi_nu[i]*gaiatrueAP[2]+Gaia_dpmura_nu[i]*gaiatrueAP[3]+Gaia_dpmudec_nu[i]*gaiatrueAP[4]);
      nll -= dnorm(Gaia_centroid_pos_al[i],gaiaAres,Gaia_centroid_pos_error_al[i],true);      
    }
 }
  
  return nll;
}


