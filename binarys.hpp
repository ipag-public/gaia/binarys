#include <TMB.hpp>
#include <iostream>

#define AU_SI       Type(149597870700.0)  /* AU -> m */
#define JY2D        Type(365.25) /* Julian years to days */
#define J2000       Type(2451545.0) /* in Julian days */
#define HIP_EPOCH   Type(1991.25)
#define DEG2RAD     Type(M_PI/180.)
#define MAS2DEG     Type(1./(1000.*3600.))
#define MAS2RAD     Type(1./(1000.*3600.)*M_PI/180.)
#define LIGHTSPEED  Type(299792458.0) /* m/s */
#define DAYSEC      Type(86400.0) // Seconds per day.
#define KAPPA       Type(4.74047)
#define HIP_MODGRIDPERIOD   Type(1207.4)
#define AZ          Type(1/(24*M_PI*JY2D/180./AU_SI));
#define TD_K        Type(6200) // TD1 param, cf. Quist & Lindegren 1999 - Eq(2)
#define TD_M1       Type(0.71) // TD1 param, cf. Quist & Lindegren 1999 - Eq(2) 
#define TD_M2       Type(0.2485) // TD1 param, cf. Quist & Lindegren 1999 - Eq(2):

/** This function allows to calculates the mass ratio when a1, M1 and period in input. */
template<class Type>
Type getqfroma1PM1(Type a1,Type periodYrs,Type M1) {
  Type a = -M1*pow(periodYrs,2)*pow(a1,-3);
  Type b= Type(1.0); Type c= Type(2.0); Type d= Type(1.0);
  Type p = -b/3./a;
  Type q = p*p*p + (b*c-3.*a*d)/(6.*a*a);
  Type r = c/3./a;
  Type s0 = q*q + pow(r-p*p,3);
  Type s1 = pow(q+ pow(s0,0.5),1./3.);
  Type s2 = pow(q- pow(s0,0.5),1./3.);
  Type ok = s1+ s2 +p;
  return ok;
}

template<class Type>
Type modulo1(Type e1) {
  return e1 - CppAD::Integer((e1));
}

/** This function resolved Kepler equation over 10 iterations */
template<class Type>
Type solveKepler(Type eccentricity, Type meanAnomaly) {
  Type eccentricAnomaly = meanAnomaly + eccentricity * sin(meanAnomaly) + eccentricity * eccentricity * sin(2 * meanAnomaly) / 2;
  for (int j = 0; j < 10; j++) {
    eccentricAnomaly += (meanAnomaly - eccentricAnomaly + eccentricity * sin(eccentricAnomaly)) / (1 - eccentricity * cos(eccentricAnomaly));
  }
  return eccentricAnomaly;
}

/** This function allows to simulate positions and radial velocities for a given time and given orbital parameters */
template<class Type>
vector<Type> relpos(Type period, Type timeperiastron, Type eccentricity, Type omega2, Type inclination, Type nodeangle, Type semiMajorAxis, Type massRatio,
                    Type time) {

  Type efac = sqrt((1.+eccentricity)/(1.-eccentricity));
  Type sinI = sin(inclination);
  Type cosI = cos(inclination);
  Type sinOmg = sin(nodeangle);
  Type cosOmg = cos(nodeangle);

  Type eccentricAnomaly = solveKepler(eccentricity, 2. * M_PI * (time - timeperiastron) / period);

  Type v = 2. * atan2(efac * sin(eccentricAnomaly / 2.), cos(eccentricAnomaly / 2.)); // true anomaly
  Type Q = v + omega2;
  Type oneminusecosE = 1 - eccentricity * cos(eccentricAnomaly);
  Type semiAmplitudeK2 = (AU_SI*1e-3*2*M_PI/DAYSEC *semiMajorAxis * sinI) /
    (period * sqrt(1 - eccentricity * eccentricity));
  Type r = semiMajorAxis * oneminusecosE;

  Type ksi2 = r*(cos(Q)*sinOmg + sin(Q)*cosOmg*cosI);
  Type eta2 = r*(cos(Q)*cosOmg - sin(Q)*sinOmg*cosI);
  // distance offset (in A.U.) along the line of sight of the component wrt the barycenter
  Type rad2 = semiMajorAxis * oneminusecosE * sin(Q) * sinI;
  Type vra2 = semiAmplitudeK2*(cos(Q)+eccentricity*cos(omega2));
  // reflex motion added to the primary
  Type Q1 = Q - M_PI;
  Type omega1 = omega2 - M_PI;
  Type primAxis = massRatio * semiMajorAxis;
  Type semiAmplitudeK1 = massRatio * semiAmplitudeK2;
  Type r1 = primAxis * oneminusecosE;
  Type ksi1 = r1*(cos(Q1)*sinOmg + sin(Q1)*cosOmg*cosI);
  Type eta1 = r1*(cos(Q1)*cosOmg - sin(Q1)*sinOmg*cosI);
  Type rad1 = primAxis * oneminusecosE * sin(Q1) * sinI;
  Type vra1 = semiAmplitudeK1*(cos(Q1)+eccentricity*cos(omega1));
  vector<Type> res(8);
  res[0] = ksi1;
  res[1] = eta1;
  res[2] = rad1;
  res[3] = vra1;
  res[4] = ksi2;
  res[5] = eta2;
  res[6] = rad2;
  res[7] = vra2;
  return res;
}

/** This function resolved IAD matrix type by giving the astrometric solution corresponding to the residual abscissas and partial derivatives */
template<class Type>
vector<Type> getIADSolution(vector<Type> Ares, vector<Type> dpra_nu, vector<Type> dpdec_nu, vector<Type> dppi_nu, vector<Type> dpmura_nu, vector<Type> dpmudec_nu) {
  // solving Amat * X = Ares
  Eigen::SparseMatrix<Type> Amat(Ares.size(),5);
  Amat.col(0) = asSparseVector(dpra_nu);
  Amat.col(1) = asSparseVector(dpdec_nu);
  Amat.col(2) = asSparseVector(dppi_nu);
  Amat.col(3) = asSparseVector(dpmura_nu);
  Amat.col(4) = asSparseVector(dpmudec_nu);
  Eigen::SimplicialLDLT< Eigen::SparseMatrix<Type> > ldlt(Amat.transpose()*Amat);
  matrix<Type> X = ldlt.solve(Amat.transpose()*asSparseVector(Ares));
  return(X.vec());
}

/** This function propagates the solution from Hipparcos to Gaia epoch. 
 This is an implementation of the equations in The Hipparcos and Tycho Catalogues (ESA SP-1200), Volume 1, Section 1.5.5, 'Epoch Transformation: Rigorous Treatment'.
 Adapted from L. Ruiz-Dern, itself adapted from L. Lindegren, itself adapted from D. Priou. */
template<class Type>
vector<Type> epochpropaFull(Type ra, Type dec, Type plx, Type pmra, Type pmdec, Type RV, Type epoch1, Type epoch2) {

  Type EPS = Type(1.0e-9);

  //Convert input data to internal units (rad, year)
  Type tau = epoch2 - epoch1;
  Type alpha0 = ra * DEG2RAD;
  Type delta0 = dec * DEG2RAD;
  Type par0 = plx * MAS2RAD;
  Type pma0 = pmra * MAS2RAD;
  Type pmd0 = pmdec * MAS2RAD;
  Type zeta0 = RV * (plx/KAPPA) * MAS2RAD;

  //Calculate normal triad [p0 q0 r0] at t0; r0 is also the unit vector to the star at epoch t0
  Type ca0 = cos(alpha0);
  Type sa0 = sin(alpha0);
  Type cd0 = cos(delta0);
  Type sd0 = sin(delta0);

  vector<Type> p0(3);
  p0[0] = -sa0;
  p0[1] = ca0;
  p0[2] = Type(0.0);

  vector<Type> q0(3);
  q0[0] = -sd0 * ca0;
  q0[1] = -sd0 * sa0;
  q0[2] = cd0;

  vector<Type> r0(3);
  r0[0] = cd0 * ca0;
  r0[1] = cd0 * sa0;
  r0[2] = sd0;

  //Proper motion vector
  vector<Type> pmv0(3);
  pmv0[0] = p0(0) * pma0 + q0(0) * pmd0;
  pmv0[1] = p0(1) * pma0 + q0(1) * pmd0;
  pmv0[2] = p0(2) * pma0 + q0(2) * pmd0;

  //Calculate distance factor f (auxiliary quantity, eq. 1.5.25)
  Type tau2 = tau * tau;
  Type pm02 = pma0 * pma0 + pmd0 * pmd0;
  Type w = Type(1.0) + zeta0 * tau;
  Type f2 = 1.0 / (1.0 + 2.0 * zeta0 * tau + (pm02 + zeta0 * zeta0) * tau2);
  Type f = sqrt(f2);
  Type f3 = f2 * f;

  //The position vector and parallax at t
  vector<Type> r(3);
  r[0] = (r0[0] * w + pmv0[0] * tau) * f;
  r[1] = (r0[1] * w + pmv0[1] * tau) * f;
  r[2] = (r0[2] * w + pmv0[2] * tau) * f;

  Type par = par0 * f;

  //The proper motion vector and normalised radial velocity at t
  vector<Type> pmv(3);
  pmv[0] = (pmv0[0] * w - r0[0] * pm02 * tau) * f3;
  pmv[1] = (pmv0[1] * w - r0[1] * pm02 * tau) * f3;
  pmv[2] = (pmv0[2] * w - r0[2] * pm02 * tau) * f3;

  Type zeta = (zeta0 + (pm02 + zeta0 * zeta0) * tau) * f2;

  //The normal triad [p q r] at t; if r is very close to the pole, select p towards RA=90 deg
  vector<Type> p(3);
  Type xy = sqrt(r[0] * r[0] + r[1] * r[1]);
  if (xy < EPS) {
    p[0] = Type(0.0);
    p[1] = Type(1.0);
    p[2] = Type(0.0);
  } else {
    p[0] = -r[1] / xy;
    p[1] = r[0] / xy;
    p[2] = Type(0.0);
  }

  vector<Type> q(3);
  q[0] = r[1] * p[2] - r[2] * p[1];
  q[1] = r[2] * p[0] - r[0] * p[2];
  q[2] = r[0] * p[1] - r[1] * p[0];

  //Convert parameters at t to external units
  Type alpha = atan2(-p[0], p[1]);
  if (alpha < 0) {
    alpha = alpha + Type(2.0 * M_PI);
  }
  Type delta = atan2(r[2], xy);
  Type pma = (p*pmv).sum();
  Type pmd = (q*pmv).sum();

  vector<Type> res(6);
  res[0] = alpha / DEG2RAD;
  res[1] = delta / DEG2RAD;
  res[2] = par / MAS2RAD;
  res[3] = pma / MAS2RAD;
  res[4] = pmd / MAS2RAD;
  res[5] = zeta / (res[2]/KAPPA) / MAS2RAD;

  return(res);
}
