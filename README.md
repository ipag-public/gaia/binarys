BINARYS (orBIt determiNAtion with Absolute and Relative astrometRY and Spectroscopy) adjusts together: the residual abscissae from Hipparcos data (IAD or TD), the 5 Gaia astrometric parameters, the DR3 NSS orbital solution or the Gaia DR4 epoch data, and complementary observations from relative astrometry and radial velocity. BINARYS uses a gradient descent method implementing automatic differentiation thanks to the R package [TMB](https://github.com/kaskr/adcomp/wiki), and it rigorously uses the information from Hipparcos and Gaia with minimal assumptions or simplifications.


<h2>Installation</h2>

- install TMB in your R environment:

        R -e 'install.packages("TMB","tmbstan")'

- compile BINARYS.cpp

        compile("BINARYS.cpp")

        dyn.load(dynlib("BINARYS"))

- use it !

        obj <- MakeADFun(data, parameters, map, DLL="BINARYS")

    data: list of data objects

    parameters: list of all parameter objects required by the user template. Enter values as starting point (example: list(a1=4,periodYrs=4.2,relTP=0.42...))

    map: non adjusted parameters, which will be fixed at the starting value (example: list(a1=factor(NA),dra=factor(NA),ddec=factor(NA)...))

        opt <- nlminb(obj$par, obj$fn, obj$gr) or opt <- do.call("optim",obj)

        rep <- sdreport(obj)

        print(rep)

- to apply MCMC

        tmbstan(obj)

<h2>Content</h2>

- BINARYS.cpp:
This file is the core of the code and returns the negative-log likelihood to be minimized.

- binarys.hpp:
This file provides constants and usefull functions for BINARYS.cpp

- DetailedDescription.md:
Description of the observations/data and parameters to be adjusted.

<h2>Changes</h2>

Version 3.0, April 2023, Gaia epoch astrometry data included, allowing to work with [Gaia BH3 data](https://aanda.org/10.1051/0004-6361/202449763).

Version 2.1, June 2023, corresponding to the paper [Chevalier et al. 2023](https://arxiv.org/abs/2307.16719):

- Gaia DR3 NSS handling implemented

- perspective acceleration during Hipparcos and Gaia observations implemented

Version 2.1 versus 2.0 corrected an issue with the HipPlxZeroPt handling and simplified it. 

Version 1.0: Mars 2022, original version used in the paper [Leclerc et al. 2023](https://ui.adsabs.harvard.edu/abs/2022arXiv220904210L/abstract)

<h2>Authors</h2>

Authors: Aurélia Leclerc and Carine Babusiaux

Reference: [Leclerc et al. 2023, Combining Hipparcos and Gaia data for the study of binaries: The BINARYS tool, A&A 672A, 82L](https://ui.adsabs.harvard.edu/abs/2022arXiv220904210L/abstract)

