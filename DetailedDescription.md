***Detailed description of the BINARYS.cpp file (v2: May 2023)***

<h1>The observations / data</h1>

<h2>Primary radial velocity</h2>
* doVr: primary radial velocity data in input or not (1 or 0)
* Vr: radial velocities of the primary in km/s
* sigVr: radial velocity errors of the primary (1 sigma) in km/s
* timeVr: observing time in Julian days from J2000
* VrEpoch: indicates different surveys - empty if all the same survey - up to 4 
surveys: VrEpoch = 1,2,3 or 4
example: for 3 different surveys, VrEpoch = 1 for the first survey and 
VrEpoch = 2 for the second and VrEpoch = 3 for the third. 
This allows 2 offsets to be adjusted (2//1 and 3//1) with VrEpoch = 1 the 
reference epoch.

<h2>Secondary radial velocity</h2>
* doVr2: secondary radial velocity data in input or not (1 or 0)
* Vr2: radial velocity of the secondary in km/s
* sigVr2: radial velocity errors of the secondary (1 sigma) in km/s
* timeVr2: observing time in Julian days from J2000
* VrEpoch2: indicates different surveys - empty if all the same survey - up to 4 
surveys : VrEpoch = 1,2,3 or 4
exemple: for 3 different survey, VrEpoch2 = 1 for the first survey and 
 VrEpoch2 = 2 for the second and VrEpoch2 = 3 for the third. 
This allows 2 offsets to be adjusted (2//1 and 3//1) with VrEpoch2 = 1 the 
reference epoch.

<h2>Relative astrometry</h2>
* doDI: relative astrometry data in input or not (1 or 0)
* ksi21: relative position (secondary with respect to the primary) in RA 
direction, in mas
* eta21: relative position (secondary with respect to the primary) in DE 
direction, in mas
* sigksi: errors (1 sigma) on the relative position 
(secondary with respect to the primary) in RA direction, in mas
* sigeta: errors (1 sigma) on the relative position 
(secondary with respect to the primary) in DE direction, in mas
* corksieta21: correlation between ksi21 and eta21
* timeDI: observing time in Julian days from J2000

<h2>Hipparcos intermediate astrometric data (IAD) </h2>
(same input for IAD original (1997) and new (2007) reductions and TD2)

* doIAD: Hipparcos IAD in input or not (1 or 0)
* dpra_nu: partial derivatives referred to ra
* dpdec_nu: partial derivatives referred to dec
* dppi_nu: partial derivatives referred to parallax
* dpmura_nu: partial derivatives referred to proper motion in ra
* dpmudec_nu: partial derivatives referred to proper motion in dec
* epoch: epoch in Julian Years with respect to Hipparcos reference epoch 1991.25
* Ares: residual abscissas in mas
* Asig: errors on residual abscissas in mas
* Hp: magnitude in Hipparcos band derived from the unmodulated part of the 
signal intensity (available in Epoch Photometry Annex)
* HipPlxZeroPt: zero-point of Hipparcos parallaxes

Reference Hipparcos solution (see the IAD or the TD header):

* Sra: ra in deg
* Sdec: dec in deg
* Splx: plx in mas
* Smualpha: proper motion in ra in mas/yrs
* Smudelta: proper motion in dec in mas/yrs

<h2>Hipparcos new transit data (TD2)</h2>
* doTD2: Hipparcos TD2 in input or not (1 or 0)

In addition to the IAD input:

* doSigFactor: take into account the sigma factor or not (1 or 0). Use 0 in a 
first time may help the convergence.
* Hip_Beta4, Hip_Beta5, Hip_sBeta4, Hip_sBeta5: amplitude and phase of the second harmonic of 
the modulated signal.
* doVAR: to take into account the variability. doVAR=0 (no variability),
 doVAR=1 (variability on the primary), doVAR=2 (variability on the secondary)
In original photometry: Epoch Photometry Annex (and Extension) - Description 
in Part 2.5 of the volume 1 Hipparcos:
* Hip_HacHdc = HpAC - Hp : for each transits, Hp is the magnitude in Hipparcos band 
derived from the unmodulated part of the signal intensity (available in Epoch 
Photometry Annex) and HpAC is the magnitude in Hipparcos band derived from the 
modulated part of the signal intensity (available in Epoch Photometry Annex Extension)
* Hip_sHacHdc = (sHp^2 + sHpAC^2)^(1/2): propagated error on HacHdc
* meanHp: mean on all Hp (magnitude in Hipparcos bandderived from the unmodulated 
part of the signal intensity) given by transits in Epoch Photometry Annex

<h2>Hipparcos original transit data (TD1)</h2>
* doTD1: Hipparcos TD1 in input or not (1 or 0)
* Hip_b1 to b5: fourier coefficient
* Hip_sb1 to sb5: error on fourier coefficient
* Hip_fx, Hip_fy and Hip_fp: spatial frequencies


<h2>Gaia DR2 or EDR3</h2>
* doGDR: 0,1 or 2. 0: no Gaia astrometric parameters in input. 1: Gaia 
astrometric parameters for the primary star. 2: Gaia astrometric parameters 
for the primary and the secondary (resolved case)
* GaiaEpoch: Gaia epoch of reference (DR2: 2015.5 - eDR3: 2016)
* solGAP: Number of Gaia astrometric parameter solution. If the solution given 
by Gaia is a 2 parameter solution: solGAP=2
* GDRAP: vector containing the 5 or 2 AP of Gaia solution: ra (deg), dec (deg), 
parallax (mas), proper motion in ra (mas/yrs), proper motion in dec (ma/yrs)
* GDRAPsig: errors (1 sigma) on the 5 or 2 AP of Gaia solution in mas and mas/yrs
* GDRcormat: correlation matrix on the 5 or 2 AP of Gaia solution
* GDRAVrUsed: Vr used by the Gaia astrometric solution for the perspective acceleration, if any.
* Gfake_time: time in the scanning law in JD since J2000

Gaia simulated partial derivatives using Gaia scan angle theta:

* Gfake_dpra_nu: sin(theta)
* Gfake_dpdec_nu: cos(theta)
* Gfake_dppi_nu: parallax factor calculated using ra and dec position and theta 
considering the orbit of the Earth.
* Gfake_dpmura_nu: Gfake_dpra_nu * time in year with respect to Gaia Epoch
* Gfake_dpmudec_nu: Gfake_dpdec_nu * time in year with respect to Gaia Epoch

Gaia B: resolved case

* solGAPB: number of Gaia astrometric parameter solution for the resolved star. 
If the solution given by Gaia is a 2 parameter solution: solGAPB=2
* GDRAPB: vector containing the 5 or 2 AP of Gaia solution of the 
resolved component: ra (deg), dec (deg), parallax (mas), proper motion 
in ra (mas/yrs), proper motion in dec (ma/yrs)
* GDRAPsigB: errors (1 sigma) on the 5 or 2 AP of Gaia resolved component 
solution in mas and mas/yrs
* GDRcormatB: correlation matrix on the 5 or 2 AP of Gaia resolved 
component solution
* GDRBVrUsed: Vr used by the Gaia astrometric solution for the perspective acceleration of the secondary, if any.

<h2> Gaia DR3 NSS</h2>
* GaiaEpoch: Gaia epoch of reference (DR3: 2016)

astrometric solution:

* bitindexA: bit_index of the astrometric solution (0 if none).
* nssAparams: relevant parameters following the bit_index order: ra, dec, parallax, pmra, pmdec, a_thiele_innes, b_thiele_innes, f_thiele_innes, g_thiele_innes, c_thiele_innes, h_thiele_innes, center_of_mass_velocity, eccentricity, period, t_periastron
! warning: for cases with fixed eccentricity (bitindex=65435), tiH should be adapted to omega=0
! warning: OrbitalAlternative and OrbitalTargeted have the same bit_index as Orbital but parameter order different, set the bit_index to -8191 for those.
* nssAcovmat: covariance matrix of the astrometric solution, same order bit_index order, as above.

Spectroscopic solution:

* bitindexS: bit_index of the spectroscopic solution (0 if none).
* nssSparams: parameters of the spectroscopic solution following the bit_index order: period, center_of_mass_velocity, semi_amplitude_primary, semi_amplitude_secondary, eccentricity, arg_periastron, t_periastron
* nssScovmat: parameters of the spectroscopic solution, same order as above.

<h2> Gaia DR4 astrometric epoch data</h2>
* GaiaEpoch: Gaia epoch of reference (DR4: 2017.5)
* doGaiaEpoch: 1 to use Gaia DR4 astrometric data, 0 otherwise.
* Gaia_time: observing time in Julian days from J2000
* Gaia_dpra_nu: partial derivative relative to ra
* Gaia_dpdec_nu: partial derivative relative to dec
* Gaia_dppi_nu: partial derivative relative to the parallax
* Gaia_dpmura_nu: partial derivative relative to the proper motion in ra
* Gaia_dpmudec_nu: partial derivative relative to the proper motion in dec
* Gaia_centroid_pos_al (along scan centroid residual, in mas)
* Gaia_centroid_pos_error_al (uncertainty on the along scan centroid residual, in mas)
* Gaia_ra0: reference RA of the epoch astrometry (in deg)
* Gaia_dec0: reference DEC of the epoch astrometry (in deg)

<h2>Option</h2>
* parset: 1 if we want to adjust M1 (primary mass) and 2 if we want to adjust
 a21 (semi major axis of the relative orbit)

<h1>Parameters to be adjusted</h1>

* a1: semi major axis of the primary orbit (in AU)
* periodYrs: period in years
* relTP: periastron time = relTP * period in JD since J2000
* ecc: orbit eccentricity 
* omega2: argument of periastron of the secondary (deg)
* inclination: inclination of the system (deg)
* nodeangle: same definition as in Hipparcos: position angle measured counterclockwise, 
as seen on the sky from the $\delta$ direction, of the intersection of the orbital 
and tangent planes. It corresponds to the position angle of the ascending node: 
when the primary star crosses the tangent plane while receding from the observer.
* par1: M1 in solar mass if parset = 1 and a21 in AU if parset = 2
* Vr0: radial velocity offset in km/s
* VrEpochShift/2/3: additionnal shifts for at the most 4 surveys in km/s. With the 
survey 1 as reference, VrEpochShift is the offset between the survey 2 and 1, 
VrEpochShift2 is the offset between survey 3 and 1 and VrEpochShift3 is the 
offset between survey 4 and 1
* VrVarJitter: radial velocity jitter (added quadratically in the errors) in km/s
* beta: fractionnal luminosity
* A1: intensity of the primary (=TD_K*10^(-mag1/2.5), with mag1 the primary magnitude in Hipparcos band)

Astrometric parameters changes with respect to the reference solution (Hipparcos or Gaia):

* dra: changes in ra (in mas)
* ddec: changes in dec (in mas)
* dplx: changes in parallax (in mas)
* dpmra: changes in proper motion in ra direction
* dpmdec: changes in proper motion in dec direction




